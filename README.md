# Requirements

Each implementation should contain following elements:

- MVVM architecture
- Androidx Lifecycle
- Jetpack Navigation Component
- Single Activity pattern
- Repository pattern
- Material design components
- Koin DI
- Retrofit and OkHttp
- Coroutines and flows
- 3rd party libs - coil/glide/picasso...

## Github App

This project contains android code for educational task Github App

[Wiki](https://gitlab.com/dice-mobile/android/template-github-task/-/wikis/home)

[Task PDF](/docs/task_github.pdf)
