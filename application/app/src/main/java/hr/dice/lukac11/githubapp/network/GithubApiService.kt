package hr.dice.lukac11.githubapp.network

import hr.dice.lukac11.githubapp.model.GithubApiModel
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApiService {
    @GET("/search/repositories")
    suspend fun getSearchResultList(@Query("q") searchInput: String, @Query("sort") star: String): GithubApiModel
}
