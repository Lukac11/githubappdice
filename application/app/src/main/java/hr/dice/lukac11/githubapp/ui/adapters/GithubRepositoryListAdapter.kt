package hr.dice.lukac11.githubapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hr.dice.lukac11.githubapp.databinding.GithubRepositoryItemBinding
import hr.dice.lukac11.githubapp.model.Items
import java.text.NumberFormat

class GithubRepositoryListAdapter(private val onRepositoryClickListener: OnRepositoryClickListener) : ListAdapter<Items, GithubRepositoryListAdapter.GithubRepositoryViewHolder>(RepositoryDiffCallback) {
    inner class GithubRepositoryViewHolder(private val binding: GithubRepositoryItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(repo: Items) {

            binding.tvRepositoryName.text = repo.fullName
            binding.tvRepositoryDescription.text = repo.description
            binding.starCount.text = NumberFormat.getNumberInstance().format(repo.stargazersCount).toString()
            itemView.setOnClickListener {
                onRepositoryClickListener.onRepositorySelected(repo)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubRepositoryViewHolder {
        val binding = GithubRepositoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GithubRepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GithubRepositoryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
