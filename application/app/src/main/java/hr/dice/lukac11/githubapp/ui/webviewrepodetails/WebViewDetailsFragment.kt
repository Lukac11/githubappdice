package hr.dice.lukac11.githubapp.ui.webviewrepodetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import hr.dice.lukac11.githubapp.databinding.FragmentWebViewDetailsBinding

class WebViewDetailsFragment : Fragment() {
    private val args: WebViewDetailsFragmentArgs by navArgs()
    private lateinit var binding: FragmentWebViewDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWebViewDetailsBinding.inflate(inflater, container, false)
        binding.webviewdetails.loadUrl(args.repourl)
        binding.webviewdetails.webViewClient = WebViewClient()
        binding.toolbarWebviewDetails.title = args.ownername
        binding.toolbarWebviewDetails.setNavigationOnClickListener {
            navigateUpOnBackstack()
        }
        return binding.root
    }
    fun navigateUpOnBackstack() {
        findNavController().navigateUp()
    }
}
