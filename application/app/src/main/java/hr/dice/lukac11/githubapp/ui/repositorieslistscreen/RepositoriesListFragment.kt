package hr.dice.lukac11.githubapp.ui.repositorieslistscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import hr.dice.lukac11.githubapp.R
import hr.dice.lukac11.githubapp.databinding.FragmentRepositoriesListBinding
import hr.dice.lukac11.githubapp.model.Items
import hr.dice.lukac11.githubapp.ui.adapters.GithubRepositoryListAdapter
import hr.dice.lukac11.githubapp.ui.adapters.OnRepositoryClickListener
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.java.KoinJavaComponent.get
import java.text.NumberFormat

class RepositoriesListFragment : Fragment(), OnRepositoryClickListener {
    private val args: RepositoriesListFragmentArgs by navArgs()
    private val repositoriesListViewModel by viewModel<RepositoriesListViewModel> { parametersOf(args.searchInput) }
    private lateinit var binding: FragmentRepositoriesListBinding
    private lateinit var githubRepositoryResultAdapter: GithubRepositoryListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRepositoriesListBinding.inflate(inflater, container, false)
        repositoriesListViewModel.githubRepositoryListSize.observe(viewLifecycleOwner) {
            binding.numberOfSearchResult.text = getString(R.string.repositoryResults, NumberFormat.getNumberInstance().format(it))
        }
        binding.rvSearchResults.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )

        githubRepositoryResultAdapter = GithubRepositoryListAdapter(this)
        binding.rvSearchResults.adapter = githubRepositoryResultAdapter
        repositoriesListViewModel.githubRepositorySearchList.observe(viewLifecycleOwner) {

            githubRepositoryResultAdapter.submitList(it)
        }

        return binding.root
    }

    override fun onRepositorySelected(item: Items) {
        navigateToRepositoryDetailsFragment(item)
    }
    fun navigateToRepositoryDetailsFragment(item: Items) {
        val action = RepositoriesListFragmentDirections.actionRepositoriesListFragmentToRepositoryDetailsFragment(item)
        findNavController().navigate(action)
    }
}
