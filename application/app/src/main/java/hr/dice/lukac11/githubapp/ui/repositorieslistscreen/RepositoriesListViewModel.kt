package hr.dice.lukac11.githubapp.ui.repositorieslistscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.githubapp.model.Items
import hr.dice.lukac11.githubapp.repository.GithubApiRepository
import kotlinx.coroutines.launch

class RepositoriesListViewModel(private val repository: GithubApiRepository, private val searchInput: String) : ViewModel() {

    private val _githubRepositorySearchList: MutableLiveData<ArrayList<Items>> = MutableLiveData<ArrayList<Items>>()
    val githubRepositorySearchList: LiveData<ArrayList<Items>> = _githubRepositorySearchList
    private val _githubRepositoryListSize: MutableLiveData<Int> = MutableLiveData<Int>()
    val githubRepositoryListSize: LiveData<Int> = _githubRepositoryListSize
    init {

        getAllGithubRepositorySearchResults(searchInput)
        getNumberOfSearchResults(searchInput)
    }

    private fun getAllGithubRepositorySearchResults(searchInput: String) {
        viewModelScope.launch {
            _githubRepositorySearchList.postValue(repository.getGithubRepositorySearchResults(searchInput).items)
        }
    }
    private fun getNumberOfSearchResults(searchInput: String) {
        viewModelScope.launch {
            _githubRepositoryListSize.postValue(repository.getGithubRepositorySearchResults(searchInput).totalCount ?: 0)
        }
    }
}
