package hr.dice.lukac11.githubapp.repository

import hr.dice.lukac11.githubapp.database.RecentGithubRepositorySearchDao
import hr.dice.lukac11.githubapp.model.RecentGithubRepositorySearch
import kotlinx.coroutines.flow.Flow

class SearchInputsRepository(private val recentGithubRepositorySearchDao: RecentGithubRepositorySearchDao) {

    suspend fun saveRecentGithubRepositorySearch(recentGithubRepositorySearch: RecentGithubRepositorySearch) {
        recentGithubRepositorySearchDao.saveRecentGithubRepositorySearch(recentGithubRepositorySearch)
    }
    suspend fun deleteRecentGithubRepositorySearch() {
        recentGithubRepositorySearchDao.deleteAllRecentGithubRepositorySearches()
    }
    fun getAllRecentGithubRepositorySearches(): Flow<List<String>> {
        return recentGithubRepositorySearchDao.getAllRecentGithubRepositoryNameSearches()
    }
}
