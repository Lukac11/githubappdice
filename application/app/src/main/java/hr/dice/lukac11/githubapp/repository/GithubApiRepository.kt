package hr.dice.lukac11.githubapp.repository

import hr.dice.lukac11.githubapp.model.GithubApiModel
import hr.dice.lukac11.githubapp.network.GithubApiService

class GithubApiRepository(private val githubApiService: GithubApiService) {

    suspend fun getGithubRepositorySearchResults(searchInput: String): GithubApiModel {
        return githubApiService.getSearchResultList(searchInput, "stars")
    }
}
