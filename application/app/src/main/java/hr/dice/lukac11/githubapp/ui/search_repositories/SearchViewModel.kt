package hr.dice.lukac11.githubapp.ui.search_repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.githubapp.model.ButtonState
import hr.dice.lukac11.githubapp.model.RecentGithubRepositorySearch
import hr.dice.lukac11.githubapp.repository.SearchInputsRepository
import kotlinx.coroutines.launch

class SearchViewModel(private val repository: SearchInputsRepository) : ViewModel() {
    private val _searchList: MutableLiveData<List<String>> = MutableLiveData<List<String>>()
    private val _buttonState: MutableLiveData<ButtonState> = MutableLiveData<ButtonState>(ButtonState())
    val searchList: LiveData<List<String>> = _searchList
    val buttonState: LiveData<ButtonState> = _buttonState
    fun saveRecentGithubRepositorySearch(recentGithubRepositorySearch: RecentGithubRepositorySearch) {
        viewModelScope.launch {
            repository.saveRecentGithubRepositorySearch(recentGithubRepositorySearch)
        }
    }
    fun deleteRecentGithubRepositorySearch() {
        viewModelScope.launch {
            repository.deleteRecentGithubRepositorySearch()
        }
    }
    fun getAllRecentGithubRepositorySearches() {
        viewModelScope.launch {
            repository.getAllRecentGithubRepositorySearches().collect {
                _searchList.value = it
            }
        }
    }

    fun onSearchInputChange(input: String) {
        _buttonState.value = buttonState.value?.copy(
            searchInput = input,
            isValidSearch = input.length> 1
        )
    }
}
