package hr.dice.lukac11.githubapp.ui.repositorydetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import hr.dice.lukac11.githubapp.R
import hr.dice.lukac11.githubapp.databinding.FragmentRepositoryDetailsBinding

class RepositoryDetailsFragment : Fragment() {
    private val args: RepositoryDetailsFragmentArgs by navArgs()
    private lateinit var binding: FragmentRepositoryDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRepositoryDetailsBinding.inflate(inflater, container, false)

        initUI()

        return binding.root
    }
    fun initUI() {
        with(binding) {
            tvOwnerName.text = args.repository.owner.login
            tvDescription.text = args.repository.description
            tvStarNumber.text = args.repository.stargazersCount.toString()
            tvIssueNumber.text = args.repository.openIssuesCount.toString()
            if (args.repository.visibility == "public") {
                tvPrivateAnswer.text = getString(R.string.no)
            } else {
                tvPrivateAnswer.text = getString(R.string.yes)
            }
            tvWatchersNumber.text = args.repository.watchersCount.toString()
            toolbarRepositoryDetails.title = args.repository.name
            ivOwnerAvatar.load(args.repository.owner.avatarUrl)
            btnOpenOwnerProfile.setOnClickListener {
                navigateFromRepositoryDetailsFragmentToWebViewDetailsFragment(args.repository.owner.htmlUrl, args.repository.owner.login)
            }
            btnOpenOwnerRepositories.setOnClickListener {
                navigateFromRepositoryDetailsFragmentToWebViewDetailsFragment(
                    args.repository.owner.htmlUrl + getString(
                        R.string.repo_link
                    ),
                    args.repository.owner.login
                )
            }
            toolbarRepositoryDetails.setNavigationOnClickListener {
                navigateFromRepositoryDetailsFragmentToRepositoriesListFragment()
            }
        }
    }
    fun navigateFromRepositoryDetailsFragmentToRepositoriesListFragment() {
        findNavController().navigateUp()
    }
    fun navigateFromRepositoryDetailsFragmentToWebViewDetailsFragment(url: String, ownerName: String) {
        val action = RepositoryDetailsFragmentDirections.actionRepositoryDetailsFragmentToWebViewDetailsFragment(url, ownerName)
        findNavController().navigate(action)
    }
}
