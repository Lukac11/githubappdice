package hr.dice.lukac11.githubapp.model

data class ButtonState(
    val searchInput: String = "",
    val isValidSearch: Boolean = false
)
