package hr.dice.lukac11.githubapp.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import hr.dice.lukac11.githubapp.model.Items

object RepositoryDiffCallback : DiffUtil.ItemCallback<Items>() {
    override fun areItemsTheSame(oldItem: Items, newItem: Items): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Items, newItem: Items): Boolean {
        return oldItem == newItem
    }
}
