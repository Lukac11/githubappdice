package hr.dice.lukac11.githubapp.ui.adapters

import hr.dice.lukac11.githubapp.model.Items

interface OnRepositoryClickListener {
    fun onRepositorySelected(item: Items)
}
