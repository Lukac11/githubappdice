package hr.dice.lukac11.githubapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey

@Entity
data class RecentGithubRepositorySearch(
    @ColumnInfo(name = "search_result", typeAffinity = OnConflictStrategy.REPLACE) @PrimaryKey
    val name: String
)
