package hr.dice.lukac11.githubapp.di

import androidx.room.Room.databaseBuilder
import hr.dice.lukac11.githubapp.Constants
import hr.dice.lukac11.githubapp.database.AppDatabase
import hr.dice.lukac11.githubapp.network.GithubApiService
import hr.dice.lukac11.githubapp.repository.GithubApiRepository
import hr.dice.lukac11.githubapp.repository.SearchInputsRepository
import hr.dice.lukac11.githubapp.ui.repositorieslistscreen.RepositoriesListViewModel
import hr.dice.lukac11.githubapp.ui.search_repositories.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val databaseModule = module {

    single { databaseBuilder(get(), AppDatabase::class.java, "SearchInputsDB").build() }
    single { get<AppDatabase>().userDao() }
}
val viewModelModule = module {
    viewModel {
        SearchViewModel(get())
    }
    viewModel {
        (searchInput: String) ->
        RepositoriesListViewModel(get(), searchInput)
    }
}
val repositoryModule = module {
    single {
        SearchInputsRepository(get())
    }
    single {
        GithubApiRepository(get())
    }
}

val networkModule = module {

    single { get<Retrofit>().create(GithubApiService::class.java) }
    single {
        Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
}
