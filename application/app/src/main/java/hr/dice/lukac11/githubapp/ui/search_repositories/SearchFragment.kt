package hr.dice.lukac11.githubapp.ui.search_repositories

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import hr.dice.lukac11.githubapp.R
import hr.dice.lukac11.githubapp.databinding.FragmentSearchBinding
import hr.dice.lukac11.githubapp.model.RecentGithubRepositorySearch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : Fragment() {
    private val searchViewModel by viewModel<SearchViewModel>()
    private lateinit var binding: FragmentSearchBinding
    private lateinit var adapter: ArrayAdapter<String>
    private val deleteDialog: AlertDialog by lazy {
        AlertDialog.Builder(requireContext()).setMessage(getString(R.string.dialogMessage)).setPositiveButton(
            getString(
                R.string.dialogPositiveButton
            )
        ) { _, _ -> searchViewModel.deleteRecentGithubRepositorySearch() }
            .setNegativeButton(getString(R.string.dialogNegativeButton)) { _, _ ->
                cancelDialog()
            }.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.unbind()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.setOnMenuItemClickListener {
            if (it.itemId == R.id.clearEntries) {
                deleteDialog.show()
                true
            } else {
                false
            }
        }
        binding.btnSearch.setOnClickListener {
            searchViewModel.saveRecentGithubRepositorySearch(RecentGithubRepositorySearch(name = binding.atvSearchinput.text.toString()))

            navigateToGithubRepositoryListFragment()
            binding.atvSearchinput.setText("")
        }
        searchViewModel.buttonState.observe(viewLifecycleOwner) {
            binding.btnSearch.isEnabled = it?.isValidSearch ?: false
        }

        binding.atvSearchinput.doOnTextChanged { input, _, _, _ ->

            searchViewModel.onSearchInputChange(input.toString())
        }

        searchViewModel.searchList.observe(
            viewLifecycleOwner
        ) {

            adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, it)
            binding.atvSearchinput.setAdapter(adapter)
        }
        searchViewModel.getAllRecentGithubRepositorySearches()
    }

    fun cancelDialog() {
        deleteDialog.dismiss()
    }
    fun navigateToGithubRepositoryListFragment() {
        val action = SearchFragmentDirections.actionSearchFragmentToRepositoriesListFragment(binding.atvSearchinput.text.toString())
        findNavController().navigate(action)
    }
}
