package hr.dice.lukac11.githubapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import hr.dice.lukac11.githubapp.model.RecentGithubRepositorySearch

@Database(entities = [RecentGithubRepositorySearch::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): RecentGithubRepositorySearchDao
}
