package hr.dice.lukac11.githubapp

import android.app.Application
import hr.dice.lukac11.githubapp.di.databaseModule
import hr.dice.lukac11.githubapp.di.networkModule
import hr.dice.lukac11.githubapp.di.repositoryModule
import hr.dice.lukac11.githubapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MyApplication)
            modules(
                databaseModule,
                viewModelModule,
                repositoryModule,
                networkModule
            )
        }
    }
}
