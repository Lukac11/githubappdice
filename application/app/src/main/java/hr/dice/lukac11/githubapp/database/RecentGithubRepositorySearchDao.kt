package hr.dice.lukac11.githubapp.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hr.dice.lukac11.githubapp.model.RecentGithubRepositorySearch
import kotlinx.coroutines.flow.Flow

@Dao
interface RecentGithubRepositorySearchDao {

    @Query("SELECT search_result FROM recentgithubrepositorysearch")
    fun getAllRecentGithubRepositoryNameSearches(): Flow<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveRecentGithubRepositorySearch(recentGithubRepositorySearch: RecentGithubRepositorySearch)

    @Query("DELETE from recentgithubrepositorysearch")
    suspend fun deleteAllRecentGithubRepositorySearches()
}
